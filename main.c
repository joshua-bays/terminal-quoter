/*
 * Joshua Bays Quoting App
 * Author: Joshua Bays
 * License: GPL2
*/

#include <ncurses.h> /*Drawing characters to screen and getting keypress input*/
#include <stdio.h> /*File opening (Could remove this header, but kept just in case need to remove ncurses for bug fixes)*/
#include <stdlib.h> /*Memory allocation, RNG*/
#include <time.h> /*Get time to set seed for RNG*/

/*Data for normal and incorrect character coloring*/
#define NormalPair 1
#define NormalTextColor COLOR_WHITE
#define NormalBgColor COLOR_BLACK
#define IncorrectPair 2 
#define IncorrectTextColor COLOR_WHITE
#define IncorrectBgColor COLOR_RED

#define DelimChar '|' /*Character used as a delimiter between the verse reference and the verse text*/
#define TargetAccuracy 90 /*The accuracy needed for the program to mark the verse as learned*/

char to_lower(char ch); /*Changes alphabetical character to be lowercase; does not affect non-alphabetical characters*/
int first_char_pos_in_str(const char* str, const char ch); /*Returns the position of the first index of a specified character; used to find where the reference ends and the verse text starts (search for DelimChar); returns -1 on absence of the character*/
unsigned len_file_lines(const char* filename); /*Returns how many lines are in a given file*/
unsigned len_arr_u(const unsigned* arr, unsigned maxSize); /*Returns the length of an array (ends at first NULL or maximum size)*/
unsigned len_str(const char* str); /*Returns the length of a char array (ends at '\0')*/
unsigned rand_u(unsigned low, unsigned high); /*Returns a random unsigned value between a given range (inclusive)*/
unsigned type_verse(const char* str); /*Has user type out verse and returns accuracy of the user's typing*/
void cpy_file_line(const char* filename, unsigned linenum, char* ret, unsigned retsize); /*Copies the content of a given line from a given file into a char array (ret)*/
void cpy_str_range(const char* src, char* dest, int start, unsigned end); /*Copies a given range of one string (src) into the other (dest)*/
void gen_no_rep_rand_seq_u(unsigned* arr, const unsigned len, const unsigned low, const unsigned high); /*Creates a random sequence of unsigned values of a given with each value occurring once at most*/
/*OPTIMIZE LATER*/ void rem_arr_elem_u(unsigned* arr, unsigned len, unsigned index); /*Removes an element from an array of unsigned values and moves every element past that one forward in the array*/

int main(int argc, char* argv[]){
	if(argc != 2){ printf("Please provide one (and only one) argument. Exiting\n"); return 1; } /*Check for CLI argument to find source file for content*/
	srand(time(0)); /*Set up RNG to be random*/
	
	WINDOW* win = initscr(); noecho(); scrollok(win, 1); /*Enable ncurses, disable showing keypresses on the display, and keep all contents in one frame (no overflow at bottom and no scrolling up to see previous contents)*/
	start_color(); init_pair(NormalPair, NormalTextColor, NormalBgColor); init_pair(IncorrectPair, IncorrectTextColor, IncorrectBgColor); /*Enable color and set color pairs for normal and incorrect characters*/
	attron(COLOR_PAIR(NormalPair)); /*Start normal color pair*/
	
	char* filename = argv[1]; /*Get source file from CLI argument*/
	unsigned len = len_file_lines(filename); /*Find length of source file (in lines) so that all lines can be read*/
	unsigned* arr = malloc(sizeof(unsigned) * len); /*Allocate enough memory to create a random sequence of indexes for file lines*/
	gen_no_rep_rand_seq_u(arr, len, 1, len); /*Turn the array into a random non-repeating sequence for file line indexes to read from the file*/
	unsigned accuracy, index; /*Accuracy will record how correctly typed the tested verse was, index records what line of the file the verse is (if accuracy meets or exceeds the target accuracy, the index will be removed from the array)*/
	
	char buff[1024]; /*Large buffer to read from the file without running out of space (the longest Bible verse is less characters than half the buffer size)*/
	while(len_arr_u(arr, len) != 0){ /*Loops until all verses have been typed*/
		index = rand_u(1, len_arr_u(arr, len)) - 1; /*Choose a random index in the unique sequence*/
		cpy_file_line(filename, arr[index], buff, sizeof(buff)); /*Copy that verse's index from the source file*/
		buff[len_str(buff) - 1] = '\0'; /*Cut off the newline character (file reading will include newline characters when reading them in C)*/
		accuracy = type_verse(buff); /*Have the user type the verse and record his or her accuracy*/
		if(accuracy >= TargetAccuracy){ rem_arr_elem_u(arr, len, index); } /*If the accuracy meets or exceeds the target accuracy, remove that verse's index from the sequence*/
		printw("Percent correct: %i%%\nPress any key to continue (%u/%u)\n\n", accuracy, len - len_arr_u(arr, len), len); /*Show the user his or her accuracy, prompt the user to provide input to go to the next verse, and then inform the user how many verses he or she has typed correctly and how many verses are in the source file*/
		getch(); /*Wait for user input*/
	}
	
	printw("Congratulations! You have typed all the verses!\nPress any key to exit\n"); /*Provide exit message and prompt the user to provide input to exit*/
	getch(); /*Wait for user input to close the program*/
	endwin(); /*Close ncurses*/
	free(arr); /*Free memory allocated to the random sequence*/
	return 0;
}

char to_lower(char ch){
	return (ch >= 'A' && ch <= 'Z') ? ch + 32 : ch; /*If capital character, turn it to a lowercase character (32 up on an ASCII table), otherwise, return the character as normal*/
}

int first_char_pos_in_str(const char* str, const char ch){
	unsigned len = len_str(str); /*Find the length of the string (done to prevent accessing any other part of the memory)*/
	for(unsigned i = 0; i <= len; i++){ /*Loop through each character of the string*/
		if(str[i] == ch){ return i; } /*Return the index of the first character to match the provided character*/
	}
	return -1; /*Return -1 if the character is not in the string*/
}

unsigned len_file_lines(const char* filename){
	FILE* readfile = fopen(filename, "r"); char buff[1024]; unsigned ret = 0;
	fgets(buff, sizeof(buff), readfile);
	while(!feof(readfile)){ fgets(buff, sizeof(buff), readfile); ret++; }
	return ret;
}

unsigned len_arr_u(const unsigned* arr, unsigned maxSize){
	unsigned ret = 0; while(arr[ret] != NULL){ ret++; }
	return (ret > maxSize) ? maxSize : ret;
}

unsigned len_str(const char* str){
	unsigned ret = 0;
	while(str[ret] != '\0'){ ret++; }
	return ret;
}

unsigned rand_u(unsigned low, unsigned high){
	if(low == high){ return low; }
	if(low > high){ unsigned tmp = low; low = high; high = tmp; }
	unsigned ret = rand() % (high + 1);
	return (ret < low) ? rand_u(low, high) : ret;
}

unsigned type_verse(const char* str){
	char ref[32]; char versetext[992]; char ch; unsigned verseSize;
	unsigned correctChars = 0; unsigned totalChars = 0;
	cpy_str_range(str, ref, 0, first_char_pos_in_str(str, DelimChar) - 1);
	cpy_str_range(str, versetext, first_char_pos_in_str(str, DelimChar) + 1, len_str(str));
	verseSize = len_str(versetext);
	printw("%s\n", ref);
	for(unsigned i = 0; i < verseSize; i++){
		switch(versetext[i]){
			case 'A' ... 'Z':
			case 'a' ... 'z':
			case '\'':
				ch = getch();
				if(to_lower(ch) == to_lower(versetext[i])){ printw("%c", versetext[i]); correctChars++; }
				else{ attron(COLOR_PAIR(IncorrectPair)); printw("%c", versetext[i]); attron(COLOR_PAIR(NormalPair)); }
				totalChars++;
				break;
			case ' ':
				do{ ch = getch(); }while(ch != versetext[i]);
				printw(" ");
				break;
			default:
				printw("%c", versetext[i]);
		}
	}
	printw("\n");
	return 100 * (float)correctChars / (float)totalChars;
}

void cpy_file_line(const char* filename, unsigned linenum, char* ret, unsigned retsize){
	unsigned counter = 1;
	FILE* readfile = fopen(filename, "r");
	fgets(ret, retsize, readfile);
	while(!feof(readfile)){
		if(counter == linenum){ return; }
		fgets(ret, retsize, readfile); counter++;
	}
	return;
}

void cpy_str_range(const char* src, char* dest, int start, unsigned end){
	if(start < 0){ return; }
	for(unsigned i = start; i <= end; i++){ dest[i - start] = src[i]; }
	dest[end - start + 1] = '\0';
	return;
}


void gen_no_rep_rand_seq_u(unsigned* arr, const unsigned len, const unsigned low, const unsigned high){
	unsigned x; _Bool inArr;
	for(unsigned i = 0; i < len; i++){
		do{
			x = rand_u(low, high); inArr = 0;
			for(unsigned j = 0; j < i; j++){ if(arr[j] == x){ inArr = 1; } }
		}while(inArr);
		arr[i] = x;
	}
}

void rem_arr_elem_u(unsigned* arr, unsigned len, unsigned index){
	unsigned arrSize = len_arr_u(arr, len);
	for(unsigned i = index; i < arrSize; i++){
		if(i != arrSize - 1){ arr[i] = arr[i + 1]; }
		else{ arr[i] = NULL; }
	}
}
